package extraction;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataExtractor {

	static String D2_API_KEY = "2858649DCF71F280BC406C0FB2D60B89";
	static String csvPath = "/home/nigel/workspace/Dota2Bot/dota2data.csv";
	
	static String matchHistoryUrl = "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=" + D2_API_KEY;
	static String matchIdUrl = "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key=" + D2_API_KEY + "&match_id=";
	
	// Helper Function for extractMatchIds
	public String getMatchHistory(){
		
		// Returns 100 of last matches
		
		StringBuilder result = new StringBuilder();

		try{	
			
			URL url = new URL(matchHistoryUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
	
//			System.out.println(result.toString());
			
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return result.toString();
		
	}
	
	public ArrayList<Long> extractMatchIds(){
		
		String matchHistoryData = this.getMatchHistory();
		JSONParser parser = new JSONParser();
		ArrayList<Long> matchIDs = new ArrayList<Long>();

		
		try {
			
			JSONObject json = (JSONObject) parser.parse(matchHistoryData);
			JSONObject jsonResult = (JSONObject) json.get("result");
			JSONArray jsonMatches = (JSONArray) jsonResult.get("matches");

			int numMatches = jsonMatches.size();
			
			for(int i = 0; i < numMatches; i++){
				JSONObject match = (JSONObject) jsonMatches.get(i);
				long matchid = (long) match.get("match_id");
				matchIDs.add(matchid);
			}
			
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		return matchIDs;
		
	}
	
	// Helper Function for extractMatchInfo
	public String getMatchId(String matchid){
		
		// Get Specific Match ID's information
		
		StringBuilder match_id_url = new StringBuilder();
		match_id_url.append(matchIdUrl);
		match_id_url.append(matchid);
		match_id_url.append("&?");
		
		StringBuilder result = new StringBuilder();
		
		try{	
			
			URL url = new URL(match_id_url.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
	
//			System.out.println(result.toString());
			
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return result.toString();
		
	}
	
	public String extractMatchInfo(String matchid){
		
		// Result translate to final result in CSV
		// match id, radiant_win, (radiant heroes), (dire heroes)
		// matchid, heroid1, heroid2, ... 
		
		StringBuilder result = new StringBuilder();
		result.append(matchid);
		result.append(",");
		
		String matchDataInfo = this.getMatchId(matchid);
		
		JSONParser parser = new JSONParser();

		try {
			
			JSONObject json = (JSONObject) parser.parse(matchDataInfo);
			JSONObject jsonResult = (JSONObject) json.get("result");
			
			// Check for lobby_type
			/* Lobby Type:
				-1	invalid
				0	Public matchmaking
				1	Practice
				2	Tournament
				3	Tutorial
				4	Co-op with AI
				5	Team match
				6	Solo queue
				7	Ranked matchmaking
				8	1v1 solo mid
				
				Only interested in 0, 2, 5, 7
			*/
			
			long lobbyType = (long) jsonResult.get("lobby_type");
			
			if(!(lobbyType == 0 || lobbyType == 2 || lobbyType == 5 || lobbyType == 7))
				return "";
						
			/* Check Game Mode
			  
			 	Game Mode
			 	0	None
				1	All Pick
				2	Captain’s Mode
				3	Random Draft
				4	Single Draft
				5	All Random
				6	Intro
				7	Diretide
				8	Reverse Captain’s Mode
				9	The Greeviling
				10	Tutorial
				11	Mid Only
				12	Least Played
				13	New Player Pool
				14	Compendium Matchmaking
				16	Captains Draft
				
				Only interested in 1, 2, 16
			 */
			long gameMode = (long) jsonResult.get("game_mode");
			if(!(gameMode == 1 || gameMode == 2 || gameMode == 16))
				return "";
						
			// Players
			JSONArray jsonPlayers = (JSONArray) jsonResult.get("players");			
			
			// Only interested in 5v5
			int numPlayers = jsonPlayers.size();
			
			if(!(numPlayers == 10))
				return "";
			
			
			// Get radiant_win
			boolean radiantWin = (boolean) jsonResult.get("radiant_win");
			if(radiantWin)
				result.append("1,");
			else
				result.append("0,");
			
			// Get Hero IDs
			for(int i = 0; i < numPlayers; i++){
				
				// Radiant then Dire
				JSONObject player = (JSONObject) jsonPlayers.get(i);
				result.append(player.get("hero_id"));
				if( i == 9)
					result.append("\n");
				else
					result.append(",");
			}
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result.toString();
		
	}
	
	public boolean writeToCsv(String csvToWrite){
		
		try{
    		File file =new File(csvPath);

    		FileWriter fileWriter = new FileWriter(file.getName(), true);
    	    BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
    	    bufferWriter.write(csvToWrite);
    	    bufferWriter.close();
    	    return true;
    	    
    	}catch(Exception e){
    		e.printStackTrace();
    	}
		
		return false;
	}
	
	
	public static void main(String[] args) {
		
		DataExtractor dataextractor = new DataExtractor();

		ArrayList<Long> matchIdList = dataextractor.extractMatchIds();
		
		for(Long matchid: matchIdList) {			
			String csvToWrite = dataextractor.extractMatchInfo(Long.toString(matchid));
			System.out.println(csvToWrite);
			dataextractor.writeToCsv(csvToWrite);
			
		}
		
		
	}
	
	// Assume 50 matches a minute
	// Test Match Id:
	// 2686151296
	
}
